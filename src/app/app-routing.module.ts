import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AboutComponent } from './components/about/about.component';
import { AboutEditComponent } from './components/about-edit/about-edit.component';
import { ArticleComponent } from './components/article/article.component';
import { ArticleEditComponent } from './components/article-edit/article-edit.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { IndexComponent } from './components/index/index.component';
import { PictureOverviewComponent } from './components/picture-overview/picture-overview.component';
import { PictureEditComponent } from './components/picture-edit/picture-edit.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginComponent } from './components/login/login.component';
import { ArticlesOverviewComponent } from './components/articles-overview/articles-overview.component';
import { ArticleAddComponent } from './components/article-add/article-add.component';
import { PictureDetailComponent } from './components/picture-detail/picture-detail.component';
import { AuthGuard } from './guards/auth.guard';

// sets the pathnames in the url to the components
// canActivate: [AuthGuard]  checks if the user is logged in and when it's not the case he guides him/she to a specific page...
// ...the login component in our case
const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'login', component: LoginComponent},
  {path: 'about', component: AboutComponent},
  {path: 'about/edit/:id', component: AboutEditComponent, canActivate: [AuthGuard]},
  {path: 'impressum', component: ImpressumComponent},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]},
  {path: 'pictures', component: PictureOverviewComponent},
  {path: 'pictures/edit', component: PictureEditComponent, canActivate: [AuthGuard]},
  {path: 'pictures/:id', component: PictureDetailComponent},
  {path: 'article/:id', component: ArticleComponent},
  {path: 'article-add', component: ArticleAddComponent, canActivate: [AuthGuard]},
  {path: 'article/edit/:id', component: ArticleEditComponent, canActivate: [AuthGuard]},
  {path: 'article', component: ArticlesOverviewComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
