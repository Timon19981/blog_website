import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private afAuth: AngularFireAuth) {  }

  canActivate(): Observable<boolean> {
    return this.afAuth.authState.map(auth => {
      if (!auth) {
        this.router.navigate(['/login']); // relocates a user who is not loggedin to the login component
        return false; // return false so you can request if a user is loggedin an what should happen next
      } else {
        return true; // return true so you can request if a user is loggedin an what should happen next
      }
    });
  }

}
