export interface User {
  name?: string;
  comment?: string;
  id?: string;
  date?: any;
  articleId?: string;
}
