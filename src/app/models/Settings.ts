// x: string; makes the attribute required
// x?: string; makes the attribute optional
export interface Settings {
  id?: string;
  hideAboutGeneral?: boolean;
  hideAboutInterests?: boolean;
  hideAboutQuestionBlog?: boolean;
  hideAboutProfilePicture?: boolean;
}
