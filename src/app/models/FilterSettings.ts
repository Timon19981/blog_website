export interface FilterSettings {
  lifestyleFilter?: boolean;
  travelFilter?: boolean;
  fashionFilter?: boolean;
}
