// x: string; makes the attribute required
// x?: string; makes the attribute optional
export interface Article {
  id?: string;
  title?: string;
  body?: string;
  lifestyle?: boolean;
  travel?: boolean;
  fashion?: boolean;
  date?: any;
  teaser?: string;
  favo?: boolean;
}
