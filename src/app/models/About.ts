// x: string; makes the attribute required
// x?: string; makes the attribute optional
export interface About {
  id?: string;
  firstName?: string;
  lastName?: string;
  general?: string;
  interests?: string;
  blogQuestion?: string;
}
