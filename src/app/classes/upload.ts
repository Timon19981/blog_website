export class Upload {
  id?: string;
  path?: any;
  date?: any;
  subtitle?: string;
  body?: string;
}
