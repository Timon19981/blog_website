import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RankingComponent } from './components/ranking/ranking.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { IndexComponent } from './components/index/index.component';
import { FirstArticlesComponent } from './components/first-articles/first-articles.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ArticlesOverviewComponent } from './components/articles-overview/articles-overview.component';
import { ArticleComponent } from './components/article/article.component';
import { PictureOverviewComponent } from './components/picture-overview/picture-overview.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ArticleEditComponent } from './components/article-edit/article-edit.component';
import { PictureEditComponent } from './components/picture-edit/picture-edit.component';
import { AboutComponent } from './components/about/about.component';
import { AboutEditComponent } from './components/about-edit/about-edit.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { FooterComponent } from './components/footer/footer.component';
import { FilterComponent } from './components/filter/filter.component';
import { ArticleAddComponent } from './components/article-add/article-add.component';
import { ArticleService } from './services/article.service';
import { AuthService } from './services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AboutService } from './services/about.service';
import { SettingsService } from './services/settings.service';
import { FilterSettingsService } from './services/filter-settings.service';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { CKEditorModule } from 'ng2-ckeditor';
import { UploadService } from './services/upload.service';
import { PictureDetailComponent } from './components/picture-detail/picture-detail.component';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RankingComponent,
    LoginComponent,
    IndexComponent,
    FirstArticlesComponent,
    NotFoundComponent,
    ArticlesOverviewComponent,
    ArticleComponent,
    PictureOverviewComponent,
    SettingsComponent,
    ArticleEditComponent,
    PictureEditComponent,
    AboutComponent,
    AboutEditComponent,
    ImpressumComponent,
    FooterComponent,
    FilterComponent,
    ArticleAddComponent,
    PictureDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'blog'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    FormsModule,
    FlashMessagesModule.forRoot(),
    CKEditorModule,
  ],
  providers: [ArticleService, AuthService, AboutService, SettingsService, FilterSettingsService, UploadService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
