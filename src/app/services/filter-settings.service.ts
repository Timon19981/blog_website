import { Injectable } from '@angular/core';
import { FilterSettings } from '../models/FilterSettings';

@Injectable()
export class FilterSettingsService {

  filterSettings: FilterSettings = {
    lifestyleFilter: false,
    travelFilter: false,
    fashionFilter: false
  };

  constructor() {
    if (localStorage.getItem('filterSettings') != null) {
      this.filterSettings = JSON.parse(localStorage.getItem('filterSettings'));
    }
  }

  getFilterSettings(): FilterSettings {
    return this.filterSettings;
  }

  updateFilterSettings(filterSettings: FilterSettings) {
    localStorage.setItem('filterSettings', JSON.stringify(filterSettings));
  }
}
