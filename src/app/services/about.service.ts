import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { About } from '../models/About';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Injectable()
export class AboutService {

  aboutsCollection: AngularFirestoreCollection<About>;
  aboutDoc: AngularFirestoreDocument<About>;
  abouts: Observable<About[]>;
  about: Observable<About>;

  constructor(private afs: AngularFirestore) {
    // fills the attribute with a list of abouts arranged by lastName
    this.aboutsCollection = this.afs.collection('abouts', ref => ref.orderBy('lastName', 'asc'));
  }

  getAbouts(): Observable<About[]> {
    // get articles with id
    this.abouts = this.aboutsCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as About;
        data.id = action.payload.doc.id;
        return data;
      });
    });
    return this.abouts;
  }

  getAbout(id: string): Observable<About> {
    // gets one about by id
    this.aboutDoc = this.afs.doc<About>(`abouts/${id}`);
    this.about = this.aboutDoc.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as About;
        data.id = action.payload.id;
        return data;
      }
    });
    return this.about;
  }

  updateAbout(about: About) {
    // updates an about by id in the database
    this.aboutDoc = this.afs.doc(`abouts/${about.id}`);
    this.aboutDoc.update(about);
  }
}
