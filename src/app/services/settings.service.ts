import { Injectable } from '@angular/core';
import { Settings } from '../models/Settings';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Injectable()
export class SettingsService {

  settingsCollection: AngularFirestoreCollection<Settings>;
  settingsDoc: AngularFirestoreDocument<Settings>;
  settings: Observable<Settings[]>;
  setting: Observable<Settings>;

  constructor(private afs: AngularFirestore) {
    this.settingsCollection = this.afs.collection('settings', ref => ref.orderBy('hideAboutGeneral', 'asc'));
  }

  getSettings(): Observable<Settings[]> {
    this.settings = this.settingsCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Settings;
        data.id = action.payload.doc.id;
        return data;
      });
    });
    return this.settings;
  }

  getSetting(): Observable<Settings> {
    this.settingsDoc = this.afs.doc<Settings>(`settings/kDcHYQupt2vyaMjJoaYx`);
    this.setting = this.settingsDoc.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Settings;
        data.id = action.payload.id;
        return data;
      }
    });
    return this.setting;
  }

  updateSettings(settings: Settings) {
    this.settingsDoc = this.afs.doc(`settings/kDcHYQupt2vyaMjJoaYx`);
    this.settingsDoc.update(settings);
  }
}
