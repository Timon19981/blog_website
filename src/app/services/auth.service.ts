import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {

  constructor(private afAuth: AngularFireAuth) { }

  login(email: string, password: string) {
    // creates a promise that the user signs in with an email address ans a password
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password).then(userData => resolve(userData), err => reject(err));
    });
  }

  getAuth() {
    // checks if the user is logged in
    return this.afAuth.authState.map(auth => auth);
  }

  logout() {
    // change the state of the user from loggedin to loggedout
    this.afAuth.auth.signOut();
  }
}
