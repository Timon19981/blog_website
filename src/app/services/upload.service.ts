import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Upload } from '../classes/upload';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';


@Injectable()
export class UploadService {

  uploadCollection: AngularFirestoreCollection<Upload>;
  uploadDoc: AngularFirestoreDocument<Upload>;
  uploads: Observable<Upload[]>;
  upload: Observable<Upload>;

  constructor(private afs: AngularFirestore) {
    this.uploadCollection = this.afs.collection('uploads', ref => ref.orderBy('date', 'desc'));
  }

  getUploads(): Observable<Upload[]> {
    this.uploads = this.uploadCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Upload;
        data.id = action.payload.doc.id;
        return data;
      });
    });
    return this.uploads;
  }

  getUpload(id: string): Observable<Upload> {
    this.uploadDoc = this.afs.doc<Upload>(`uploads/${id}`);
    this.upload = this.uploadDoc.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Upload;
        data.id = action.payload.id;
        return data;
      }
    });
    return this.upload;
  }

  newUpload(upload: Upload) {
    const data = JSON.parse(JSON.stringify(upload));
    this.uploadCollection.add(data);
  }
}
