import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Article } from '../models/Article';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Injectable()
export class ArticleService {

  articlesCollection: AngularFirestoreCollection<Article>;
  articlesCollectionDate: AngularFirestoreCollection<Article>;
  articleDoc: AngularFirestoreDocument<Article>;
  articles: Observable<Article[]>;
  article: Observable<Article>;

  constructor(private afs: AngularFirestore) {
    // fills the attribute with a list of articles arranged by title
    this.articlesCollection = this.afs.collection('articles', ref => ref.orderBy('title', 'asc'));
    this.articlesCollectionDate = this.afs.collection('articles', ref => ref.orderBy('date', 'desc')); // .limit(2) limit the array to 2
  }

  getArticles(): Observable<Article[]> {
    // get articles with id sorted by title
    this.articles = this.articlesCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Article;
        data.id = action.payload.doc.id;
        return data;
      });
    });
    return this.articles;
  }

  // get articles with id sorted by creation date
  getArticlesDate(): Observable<Article[]> {
    this.articles = this.articlesCollectionDate.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Article;
        data.id = action.payload.doc.id;
        return data;
      });
    });
    return this.articles;
  }

  newArticle(article: Article) {
    // adds a new article to the database
    this.articlesCollection.add(article);
  }

  getArticle(id: string): Observable<Article> {
    // gets one article by id
    this.articleDoc = this.afs.doc<Article>(`articles/${id}`);
    this.article = this.articleDoc.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Article;
        data.id = action.payload.id;
        return data;
      }
    });
    return this.article;
  }

  updateArticle(article: Article) {
    // updates an article by id in the database
    this.articleDoc = this.afs.doc(`articles/${article.id}`);
    this.articleDoc.update(article);
  }

  deleteArticle(article: Article) {
    // deletes an article by id in the database
    this.articleDoc = this.afs.doc(`articles/${article.id}`);
    this.articleDoc.delete();
  }
}
