import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/Article';
import { CKEditorModule } from 'ng2-ckeditor';

@Component({
  selector: 'app-article-add',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.css']
})
export class ArticleAddComponent implements OnInit {

  article: Article;
  lifestyle: Boolean = false;
  travel: Boolean = false;
  fashion: Boolean = false;

  @ViewChild('articleForm') form: any;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private articleService: ArticleService
  ) { }

  ngOnInit() {}

  onSubmit({value, valid}: {value: Article, valid: boolean}) {
    if (!valid) {
      // Show error
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      // sets a date to the article
      value.date = new Date();
      // marks the article as a favorite to be displayed on the main page
      value.favo = false;
      // Add new article
      this.articleService.newArticle(value);
      // Show success
      this.flashMessage.show('New article added', {
        cssClass: 'alert-success', timeout: 4000
      });
      // Redirect to article-overview
      this.router.navigate(['/article']);
    }
  }
}
