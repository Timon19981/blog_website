import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Router } from '@angular/router';
import { FilterSettings } from '../../models/FilterSettings';
import { FilterSettingsService } from '../../services/filter-settings.service';

@Component({
  selector: 'app-articles-overview',
  templateUrl: './articles-overview.component.html',
  styleUrls: ['./articles-overview.component.css']
})
export class ArticlesOverviewComponent implements OnInit {

  articles: Article[];
  lifestyleFilter: boolean;
  travelFilter: boolean;
  fashionFilter: boolean;

  constructor(private articleService: ArticleService, private router: Router, private filterSettingsService: FilterSettingsService) { }

  ngOnInit() {
    // gets the array of articles
    this.articleService.getArticles().subscribe(articles => {
      this.articles = articles;
    });

    this.lifestyleFilter = this.filterSettingsService.getFilterSettings().lifestyleFilter;
    this.travelFilter = this.filterSettingsService.getFilterSettings().travelFilter;
    this.fashionFilter = this.filterSettingsService.getFilterSettings().fashionFilter;
  }

  getFilterArticles() {
    // changes the array of the articles to specific situations
    if (this.lifestyleFilter && this.travelFilter && this.fashionFilter) {
      return this.articles.filter((article) => article);
    }
    if (this.lifestyleFilter === true) {
      return this.articles.filter((article) => article.lifestyle);
    }
    if (this.travelFilter === true) {
      return this.articles.filter((article) => article.travel);
    }
    if (this.fashionFilter === true) {
      return this.articles.filter((article) => article.fashion);
    }
    if (this.lifestyleFilter === false && this.fashionFilter === false && this.travelFilter === false) {
      return this.articles.filter((article) => article);
    }
  }
}
