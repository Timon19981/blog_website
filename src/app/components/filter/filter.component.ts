import { Component, OnInit, Input, NgZone } from '@angular/core';
import { FilterSettings } from '../../models/FilterSettings';
import { FilterSettingsService } from '../../services/filter-settings.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  filterHide: boolean;
  filterSettings: FilterSettings;

  @Input() travel: boolean;
  @Input() lifestyle: boolean;
  @Input() fashion: boolean;

  constructor(private filterSettingsService: FilterSettingsService, private router: Router, private zone: NgZone) { }

  ngOnInit() {
    this.filterSettings = this.filterSettingsService.getFilterSettings();
  }

  changeFilterSettings() {
    this.filterSettingsService.updateFilterSettings(this.filterSettings);
    this.zone.runOutsideAngular(() => {
      location.reload();
    });
  }

  showFilter() {
    if (!this.filterHide) {
      this.filterHide = true;
    } else {
      this.filterHide = false;
    }
  }
}
