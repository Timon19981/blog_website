import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { About } from '../../models/About';
import { AboutService } from '../../services/about.service';
import { SettingsService } from '../../services/settings.service';
import { Settings } from '../../models/Settings';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  isLoggedIn: boolean;
  abouts: About[];
  id: string;
  settings: Settings;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private aboutService: AboutService,
    private settingsService: SettingsService
  ) { }

  ngOnInit() {
    // checks if the user is logged in or not so he/she can see the edit button
    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
    // takes the id from the data base
    this.id = this.route.snapshot.params['id'];

    // gets through the id the data from the database
    this.aboutService.getAbouts().subscribe(abouts => {
      this.abouts = abouts;
    });

    this.settingsService.getSetting().subscribe(settings => {
      this.settings = settings;
    });
  }
}
