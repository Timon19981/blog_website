import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/Article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  article: Article;
  users: User[];
  user: User;
  id: string;
  isLoggedIn: boolean;

  constructor(
    private articleService: ArticleService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

  ngOnInit() {
    // gets the id of the article
    this.id = this.route.snapshot.params['id'];

    // gets the content of the article by id
    this.articleService.getArticle(this.id).subscribe(article => {
      this.article = article;
    });

    // get the content of the comments by id
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });

    // checks if a user is loggedin
    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  // fucntion to save a comment
  onSubmit({value, valid}: {value: User, valid: boolean}) {
    if (!valid) {
      // Show error
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      value.date = new Date(); // set a date to the comment
      value.articleId = this.id;  // save the id of the article
      this.userService.newUser(value); // the the comment
      this.flashMessage.show('New comment added', {
        cssClass: 'alert-success', timeout: 4000
      });
    }
  }

  onDeleteClick() {
    if (confirm('Are you sure you want to delete this article?')) {
      this.articleService.deleteArticle(this.article);
      this.flashMessage.show('Article removed', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/article']);
    }
  }
}
