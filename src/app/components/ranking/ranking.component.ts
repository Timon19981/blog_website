import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

  articles: Article[];
  articlesByDate: Article[];
  showTitle: boolean;
  btnTitle: string;
  showMenuTitleToday: Boolean = false;
  showMenuTitleMonth: Boolean = true;
  showMenuTitleYear: Boolean = true;

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    // gets the array of articles
    this.articleService.getArticles().subscribe(articles => {
      this.articles = articles;
    });
    // gets the array of articles sorted by creation date
    this.articleService.getArticlesDate().subscribe(articles => {
      this.articlesByDate = articles;
    });

    this.btnTitle = 'Today';
  }

  switchToMonth() {
    this.showMenuTitleMonth = false;
    this.showMenuTitleToday = true;
    this.showMenuTitleYear = true;
    this.showTitle = true;
    this.btnTitle = 'Month';
  }

  switchToToday() {
    this.showMenuTitleMonth = true;
    this.showMenuTitleToday = false;
    this.showMenuTitleYear = true;
    this.showTitle = false;
    this.btnTitle = 'Today';
  }

  switchToYear() {
    this.showMenuTitleMonth = true;
    this.showMenuTitleToday = true;
    this.showMenuTitleYear = false;
    this.showTitle = false;
    this.btnTitle = 'Year';
  }
}
