import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { About } from '../../models/About';
import { AboutService } from '../../services/about.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: boolean;
  abouts: About[];
  // id: 'XkUNDuao9mUSTH8OmehE';

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private aboutService: AboutService
  ) {}

  ngOnInit() {

    this.aboutService.getAbouts().subscribe(abouts => {
      this.abouts = abouts;
    });

    // checks if the user is loggedin
    this.authService.getAuth().subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  onLogoutClick() {
    // logges the user out
    this.authService.logout();
    this.flashMessage.show('You are now logged out!', {
      cssClass: 'alert-success', timeout: 4000
    });
    // relocates the user to the login.component
    this.router.navigate(['/login']);
  }
}
