import { Component, OnInit } from '@angular/core';
import { collectExternalReferences } from '@angular/compiler';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UploadService } from '../../services/upload.service';
import { Upload } from '../../classes/upload';

@Component({
  selector: 'app-picture-edit',
  templateUrl: './picture-edit.component.html',
  styleUrls: ['./picture-edit.component.css']
})
export class PictureEditComponent implements OnInit {
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  snapshot: Observable<any>;
  downloadURL: Observable<string>;
  isHovered: boolean;
  upload: Upload;
  filePath: string;
  picture: string;

  constructor(private afStoarge: AngularFireStorage,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private uploadService: UploadService
  ) {}

  ngOnInit() {}

  uploadEvent(event) {
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStoarge.ref(id);
    this.task = this.ref.put(event.target.files[0]);
    this.downloadURL = this.task.downloadURL();
  }

  onSubmit({value, valid}: {value: Upload, valid: boolean}) {
    if (!valid) {
      // Show error
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      this.downloadURL.subscribe(data => {
        this.filePath = data;
        this.upload = new Upload();
        this.upload.date = new Date();
        this.upload.path = this.filePath;
        this.upload.body = value.body;
        this.upload.subtitle = value.subtitle;
        this.uploadService.newUpload(this.upload);
      });
      this.flashMessage.show('New Picture added', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/pictures']);
    }
  }
}
