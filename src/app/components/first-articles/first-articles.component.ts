import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';

@Component({
  selector: 'app-first-articles',
  templateUrl: './first-articles.component.html',
  styleUrls: ['./first-articles.component.css']
})
export class FirstArticlesComponent implements OnInit {

  articles: Article[];
  article: Article;
  articlesByDate: Article[];

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articleService.getArticles().subscribe(articles => {
      this.articles = articles;
    });

    this.articleService.getArticlesDate().subscribe(articles => {
      this.articlesByDate = articles;
    });
  }
}
