import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/Article';
import { ArticleService } from '../../services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {

  article: Article;
  lifestyle: Boolean = false;
  travel: Boolean = false;
  fashion: Boolean = false;
  id: string;

  constructor(
    private articleService: ArticleService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    // gets the id of the article
    this.id = this.route.snapshot.params['id'];

    // gets content of the article from the database
    this.articleService.getArticle(this.id).subscribe(article => {
      this.article = article;
    });
  }

  onSubmit({value, valid}: {value: Article, valid: boolean}) {
    if (!valid) {
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      // Add id to article
      value.id = this.id;
      // update article
      this.articleService.updateArticle(value);
      this.flashMessage.show('Article updated', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/article/' + this.id]);
    }
  }
}
