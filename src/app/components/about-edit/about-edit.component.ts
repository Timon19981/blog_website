import { Component, OnInit } from '@angular/core';
import { About } from '../../models/About';
import { AboutService } from '../../services/about.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SettingsService } from '../../services/settings.service';
import { Settings } from '../../models/Settings';

@Component({
  selector: 'app-about-edit',
  templateUrl: './about-edit.component.html',
  styleUrls: ['./about-edit.component.css']
})
export class AboutEditComponent implements OnInit {

  about: About;
  id: string;
  settings: Settings;

  constructor(
    private aboutService: AboutService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessage: FlashMessagesService,
    private settingsService: SettingsService
  ) { }

  ngOnInit() {
    // takes the id from the data base
    this.id = this.route.snapshot.params['id'];

    // gets through the id the data from the database
    this.aboutService.getAbout(this.id).subscribe(about => {
      this.about = about;
    });

    this.settingsService.getSetting().subscribe(settings => {
      this.settings = settings;
    });
  }

  onSubmit({value, valid}: {value: About, valid: boolean}) {
    // if statement to check if the form is valid
    if (!valid) {
      // if the form isn't valid a message will be shown for 4 seconds
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      // Add id to the about data in the database
      value.id = this.id;
      // update about data in the database
      this.aboutService.updateAbout(value);
      this.flashMessage.show('About updated', {
        cssClass: 'alert-success', timeout: 4000
      });
      this.router.navigate(['/about']);
    }
  }
}
