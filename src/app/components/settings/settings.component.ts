import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { Settings } from '../../models/Settings';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  settings: Settings;

  constructor(
    private settingsService: SettingsService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    // gets the settings
    this.settingsService.getSetting().subscribe(settings => {
      this.settings = settings;
    });
  }

  onSubmit() {
    // change the settings and display a message
    this.settingsService.updateSettings(this.settings);
    this.flashMessage.show('Settings saved!', {
      cssClass: 'alert-success', timeout: 4000
    });
  }
}
