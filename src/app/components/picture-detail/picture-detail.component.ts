import { Component, OnInit } from '@angular/core';
import { Upload } from '../../classes/upload';
import { UploadService } from '../../services/upload.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-picture-detail',
  templateUrl: './picture-detail.component.html',
  styleUrls: ['./picture-detail.component.css']
})
export class PictureDetailComponent implements OnInit {

  upload: Upload;
  id: string;
  users: User[];

  constructor(
    private uploadService: UploadService,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.uploadService.getUpload(this.id).subscribe(upload => {
      this.upload = upload;
    });

    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });
  }

  // fucntion to save a comment
  onSubmit({value, valid}: {value: User, valid: boolean}) {
    if (!valid) {
      // Show error
      this.flashMessage.show('Please fill out the form correctly', {
        cssClass: 'alert-danger', timeout: 4000
      });
    } else {
      value.date = new Date(); // set a date to the comment
      value.articleId = this.id;  // save the id of the article
      this.userService.newUser(value); // the the comment
      this.flashMessage.show('New comment added', {
        cssClass: 'alert-success', timeout: 4000
      });
    }
  }
}
