import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  // adds and removes css classes to elements when the normal footer should be shown at a specific screensize
  showFooter() {
    $('#footer').removeClass('trytohide');
    $('#footerBtn').addClass('trytohide');
    $('#exit-btn').removeClass('only-small');
  }
  // adds and removes css classes to elements when the normal footer should be hidden at a specific screensize
  hideFooter() {
    $('#footer').addClass('trytohide');
    $('#footerBtn').removeClass('trytohide');
    $('#exit-btn').addClass('only-small');
  }
}
