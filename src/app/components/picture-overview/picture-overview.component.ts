import { Component, OnInit } from '@angular/core';
import { UploadService } from '../../services/upload.service';
import { Upload } from '../../classes/upload';

@Component({
  selector: 'app-picture-overview',
  templateUrl: './picture-overview.component.html',
  styleUrls: ['./picture-overview.component.css']
})
export class PictureOverviewComponent implements OnInit {

  uploads: Upload[];

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
    this.uploadService.getUploads().subscribe(uploads => {
      this.uploads = uploads;
    });
  }
}
