// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC7XQTzddAozPT9E5EACgCaClNkTJSORXw',
    authDomain: 'blog-b4ee6.firebaseapp.com',
    databaseURL: 'https://blog-b4ee6.firebaseio.com',
    projectId: 'blog-b4ee6',
    storageBucket: 'blog-b4ee6.appspot.com',
    messagingSenderId: '521419049248'
  }
};
